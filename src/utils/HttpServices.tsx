import axios from "axios";

interface HttpServicesParams {
    endpoint: string;
    body?: object;
    hasAuthToken?: boolean;
}


export class HttpServices {
    private static baseURL = 'http://18.218.1.26/api/v1/';
    static async post({ endpoint, body, hasAuthToken }: HttpServicesParams): Promise<any> {
        const response = await fetch(`${this.baseURL}${endpoint}`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                ...hasAuthToken ? { 'Authorization': `Bearer ${localStorage.getItem('user')}` } : {}
            }
        });
        let dataResponse;
        try {
            dataResponse = await response.json();
        } catch (error) {
            dataResponse = {};
        }
        return {
            data: dataResponse,
            ok: response.ok,
            status: response.status
        }
    }

    static async get({ endpoint, hasAuthToken }: HttpServicesParams): Promise<any> {
        try {
            const response = await axios.get(`${this.baseURL}${endpoint}`, {
            });
            return response.data;
        } catch (error) {
            return error;
        }
    }

    static async put({ endpoint, body, hasAuthToken }: HttpServicesParams): Promise<any> {
        try {
            const response = await axios.put(`${this.baseURL}${endpoint}`, body ? body : null, {
                headers: {
                    "Accept": "application/json",
                    "Content-type": "application/json",
                }
            });
            return response;
        } catch (error) {
            return error;
        }
    }

    static async del({ endpoint, body, hasAuthToken }: HttpServicesParams): Promise<any> {
        console.log(`${this.baseURL}${endpoint}`);
        const response = await fetch(`${this.baseURL}${endpoint}`, {
            method: 'DELETE',
            body: JSON.stringify(body),
            headers: {
                'Content-Type': 'application/json',
                ...hasAuthToken ? { 'Authorization': `Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjVmZTZhOGJjODBkMWRmMDAxMjRkNWM0MiIsImVtYWlsIjoidGVzdEB0ZXN0LmNvbSIsIm5hbWUiOiJOb21icmUiLCJpYXQiOjE2MDg5NTIwMDR9.1KYDRxIS-dvpY_Q64fZp_4MNgdaJR_0sMSaiJzEiEsw` } : {}
            }
        });
        let dataResponse;
        try {
            dataResponse = await response.json();
        } catch (error) {
            console.log(response);
            dataResponse = {};
        }
        return {
            data: dataResponse,
            ok: response.ok,
            status: response.status
        }
    }
}

interface APIResponse {
    data: any;
    ok: boolean;
    code: number;
}