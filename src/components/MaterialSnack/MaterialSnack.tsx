import { IconButton, Snackbar } from '@material-ui/core';
import React from 'react';
import CloseIcon from '@material-ui/icons/Close';
export default function MaterialSnack(props: any) {

    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };
    return (
        <Snackbar
            anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
            open={props.open}
            onClose={props.handleClose}
            message={props.message}
            action={
                <React.Fragment>
                    <IconButton
                        aria-label="close"
                        color="inherit"
                        style={{ padding: 1 }}
                        onClick={props.handleClose}
                    >
                        <CloseIcon />
                    </IconButton>
                </React.Fragment>
            }
        />
    );
}