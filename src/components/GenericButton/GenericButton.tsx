import { Button } from '@material-ui/core';
import React, { FC } from 'react';
import { makeStyles, Theme, Typography } from '@material-ui/core';

type ButtonProps = {
    color: string,
    text: string,
    width: string,
    toDo? : any
}

const useStyles = makeStyles((theme: Theme) => ({
    button: (props: ButtonProps) => {
        return ({
            backgroundColor: props.color,
            color: 'white',
            width: props.width,
            '&:hover': {
                background: "grey",
             },
        });
    }
}));

const GenericButton: FC<ButtonProps> = (props) => {
    const classes = useStyles(props);
    return (
        <Button className={classes.button} onClick={props.toDo}>{props.text}</Button>
    );
}


export default GenericButton;