import React from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, Grid, TextField, Typography } from '@material-ui/core';
import { HttpServices } from '../../../utils/HttpServices';
import axios from 'axios';
import MaterialSnack from '../../../components/MaterialSnack/MaterialSnack';


const useStyles = makeStyles(
    createStyles({
        item: {
            backgroundImage: (props: any) => props.info.image !== "" ? `url(${props.info.image})` : `url(https://i.pinimg.com/736x/91/54/ba/9154baa627dcfcf87b0d936cfcecac9d.jpg)`,
            /*  backgroundImage: (props: any) => `url(https://i.pinimg.com/736x/91/54/ba/9154baa627dcfcf87b0d936cfcecac9d.jpg)`, */
            width: '100%',
            height: '100%',
            borderRadius: 5,
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: 'center',
        },
        container: {
            borderRadius: 5,
            cursor: 'pointer',
            /* border: (props: any) => props.info.status == 'ready' ? '0px solid green' : '0px solid orange',
            boxShadow: (props: any) => props.info.status == 'ready' ? '0px 0px 20px -1px rgba(0,128,0,1)' : '0px 0px 20px -1px rgba(255,165,0,1);' */
        },
    }),
);

export default function Item(props: any) {
    const classes = useStyles(props);
    const [open, setOpen] = React.useState(false);
    const [desc, setDesc] = React.useState("")
    const [color, setColor] = React.useState(props.info.status)
    const [selectedDate, setSelectedDate] = React.useState("2020-05-13");
    const [openAlert, setOpenAlert] = React.useState(false);
    const [message, setMessage] = React.useState("");
    const handleAlert = () => {
        setOpenAlert(!openAlert);
    };
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setDesc("");
        setOpen(false);
    };


    const updateStatus = async () => {
        if (props.info.status === "ready") {
            const body = {
                "description": desc,
                "estimatedDate": selectedDate,
            }
            HttpServices.put({ endpoint: `vehicles/${props.info._id}/maintenance`, body, hasAuthToken: false }).then((res) => {
                console.log(res.data.data);
                if (res.data.data.status !== undefined) {
                    console.log(res.data.data.status);
                    var aux = props.vehicles;
                    aux[props.i].status = res.data.data.status;
                    props.setVehicles(aux);
                    setColor(res.data.data.status);
                }
                setMessage("información guardada con éxito");
                handleAlert();
            }).catch((err) => {
                console.log(err);
                setMessage("Error al guardar la informacion");
                handleAlert();
            });
        } else {
            const body = {}
            HttpServices.put({ endpoint: `vehicles/${props.info._id}/ready`, hasAuthToken: false }).then((res) => {
                console.log(res);
                if (res.data.data.status !== undefined) {
                    console.log(res.data.data.status);
                    var aux = props.vehicles;
                    aux[props.i].status = res.data.data.status;
                    props.setVehicles(aux);
                    setColor(res.data.data.status);
                }
                setMessage("información guardada con éxito");
                handleAlert();
            }).catch((err) => {
                setMessage("Error al guardar la informacion");
                handleAlert();
            })
        }
        handleClose();
    }
    return (

        <div style={{ height: '100%' }}>
            <Grid container style={{
                height: '100%',
                width: '100%',
                border: color == 'ready' ? '0px solid green' : '0px solid orange',
                boxShadow: color == 'ready' ? '0px 0px 20px -1px rgba(0,128,0,1)' : '0px 0px 20px -1px rgba(255,165,0,1)'
            }} className={classes.container} onClick={handleClickOpen}>
                <Grid item xs={12} style={{ height: '70%' }}>
                    <div className={classes.item} >

                    </div>
                </Grid>
                <Grid item xs={12} style={{ paddingLeft: 10 }}>
                    <Typography>
                        Marca: {props.info.make}
                    </Typography>
                </Grid>
                <Grid item xs={12} style={{ paddingLeft: 10 }}>
                    <Typography>
                        Modelo: {props.info.model}
                    </Typography>
                </Grid>
            </Grid>
            <div>
                <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                    <DialogContent>
                        <DialogContentText>
                            {
                                props.info.status == "ready"
                                    ?
                                    'Vehículo actualmente disponible, si desea ingresarlo a mantenimiento por favor ingrese una fecha estimada y una descripción'
                                    :
                                    'Vehículo actualmente en mantenimiento, si este ya finalizó por favor cambie su estado a disponible'
                            }
                        </DialogContentText>
                        {props.info.status == "ready"
                            &&
                            <Grid container spacing={3}>
                                <Grid item xs={12}>
                                    <TextField
                                        id="date"
                                        label="Fecha estimada"
                                        type="date"
                                        defaultValue="2020-12-29"
                                        onChange={(e) => { setSelectedDate(e.target.value) }}
                                        InputLabelProps={{
                                            shrink: true,
                                        }}
                                    />
                                </Grid>
                                <Grid item xs={12}>
                                    <TextField
                                        fullWidth={true}
                                        id="outlined-multiline-static"
                                        label="Descripción"
                                        multiline
                                        rows={4}
                                        onChange={(e) => { setDesc(e.target.value) }}
                                        variant="outlined"
                                    />
                                </Grid>
                            </Grid>}
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={handleClose} color="primary">
                            Cancelar
                        </Button>
                        <Button onClick={updateStatus} variant="contained" color="primary">
                            Aceptar
                        </Button>
                    </DialogActions>
                </Dialog>
                <MaterialSnack open={openAlert} handleClose={handleAlert} message={message} />
            </div>
        </div>
    );
}