import { Button, FormControl, Grid, IconButton, Input, InputAdornment, Typography } from '@material-ui/core';
import React, { useEffect, useRef, useState } from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import GridOnIcon from '@material-ui/icons/GridOn';
import ReorderIcon from '@material-ui/icons/Reorder';
import GalleryItem from './Item';
import { HttpServices } from '../../../utils/HttpServices';
import axios from 'axios';
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        icon: {
            width: 150,
            height: 100
        },
        myGrid: {
            padding: 30,
            display: 'grid',
            gridTemplateRows: '1fr',
            gridGap: 30,
        },
    }),
);

function MainContainer() {
    const classes = useStyles();
    const [listView, setListView] = useState(true);
    const [loading, setLoading] = useState(true);
    const [vehicles, setVehicles] = useState([]);
    const elementRef = useRef<any>(null);
    const [aux, setAux] = useState<number>(30);
    const handleResize = () => {
        setAux(elementRef.current?.clientWidth);
    }

    useEffect(() => {
        window.addEventListener("resize", handleResize);
        setAux(elementRef.current?.clientWidth);
        return () => {
            window.removeEventListener("resize", handleResize);
        }
    })


    // get cars
    async function getVehicles() {
        HttpServices.get({ endpoint: 'vehicles', hasAuthToken: true }).then((res) => {
            setVehicles(res.data)
        }).catch((err) => {
            console.log(err);
        })
    }
    useEffect(() => {
        getVehicles();
    }, []);

    useEffect(() => {
        console.log(vehicles);
    }, [vehicles]);

    return (
        <div style={{ padding: 30 }}>
            <Grid container spacing={5}>
                <Grid item xs={8}>
                    <Grid container spacing={3} direction="row"
                        justify="flex-start"
                        alignItems="center">
                        <Grid item xs={12} sm={8}>
                            <Typography variant="h3">
                                Vehículos actuales
                            </Typography>
                        </Grid>
                        <Grid item xs={2} sm={2}>
                            <IconButton
                                onClick={() => setListView(false)}
                                style={{ color: !listView ? 'black' : 'grey' }}
                            >
                                <ReorderIcon fontSize="large" />
                            </IconButton>
                        </Grid>
                        <Grid item xs={2} sm={2}>
                            <IconButton
                                size="medium"
                                onClick={() => setListView(true)}
                                style={{ color: listView ? 'black' : 'grey' }}
                            >
                                <GridOnIcon fontSize="large" />
                            </IconButton>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item xs={12}>
                    <Grid container justify="flex-start"
                        alignItems="center" className={classes.myGrid} style={{ gridTemplateColumns: listView ? '1fr 1fr 1fr 1fr 1fr' : '1fr 1fr 1fr' }}>
                        {
                            vehicles.map((item, index) => (
                                index === 0
                                    ?
                                    <div ref={elementRef} >
                                        <Grid item xs style={{ height: aux }}>
                                            <GalleryItem info={item} vehicles={vehicles} setVehicles={setVehicles} i={index} />
                                        </Grid>
                                    </div>
                                    :
                                    <Grid item xs style={{ height: aux }}>
                                        <GalleryItem info={item} vehicles={vehicles} setVehicles={setVehicles} i={index} />
                                    </Grid>
                            ))
                        }
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}

export default MainContainer;