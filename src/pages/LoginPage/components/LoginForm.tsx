import { Button, Grid, TextField, Typography } from '@material-ui/core';
import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import GenericButton from '../../../components/GenericButton/GenericButton';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import MaterialSnack from '../../../components/MaterialSnack/MaterialSnack';
import { timeout } from '../../../utils/RedirectTime';
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        textField: {
            width: '70%'
        },
        item: {
            textAlign: 'center',
            padding: '15px 0px 0px 0px'
        },
        forgot: {
            textAlign: 'end',
            display: 'flex',
            justifyContent: 'center',
            padding: '15px 0px 0px 0px',
        },
        primaryButton: {
            marginTop: '1em',
            width: '70%'
        },
        companyName: {
            fontWeight: 'bold',
            padding: '15px 0px 0px 0px',
            textAlign: 'center'
        },
        link: {
            color: 'black',
            /*  &:focus, &:hover, &:visited, &:link, &:active {
                 text-decoration: none; */
            '&:link': {
                textDecoration: 'none'
            },

        }
    }),
);

export default function LoginForm() {
    const classes = useStyles();
    const [email, setEmail] = React.useState("");
    const [password, setPassword] = React.useState("");
    const [openAlert, setOpenAlert] = React.useState(false);
    const [message, setMessage] = React.useState("");
    const handleAlert = () => {
        setOpenAlert(!openAlert);
    };
    const history = useHistory();

    const validate = async () => {
        var body = {
            "email": email,
            "password": password
        }
        var response = axios.post('http://18.218.1.26/api/auth/login', body, {
            headers: {
                "Accept": "application/json",
                "Content-type": "application/json",
            }
        }).then((res) => {
            setMessage("Bienvenido...");
            handleAlert();
            timeout(2000).then(()=>history.push('/vehicles'));
        }).catch((err) => {
            setMessage("Usuario o contraseña incorrecta, intente nuevamente");
            handleAlert();
        })
    }

    return (
        <Grid container direction="row"
            justify="center"
            alignItems="center" >
            <Grid item xs={12} className={classes.companyName}>
                <Typography variant="h3" display="inline" style={{ fontWeight: 'bold' }}>
                    Cars Manager
                </Typography>
            </Grid>
            <Grid item xs={12} className={classes.item}>
                <TextField
                    id="outlined-basic"
                    className={classes.textField}
                    onChange={(e) => { setEmail(e.target.value) }}
                    label="Correo"
                    margin="normal"
                    variant="outlined"
                />
            </Grid>
            <Grid item xs={12} className={classes.item}>
                <TextField
                    id="outlined-basic"
                    className={classes.textField}
                    label="Contraseña"
                    margin="normal"
                    type="password"
                    variant="outlined"
                    onChange={(e) => { setPassword(e.target.value) }}
                />
            </Grid>
            <Grid item xs={12} className={classes.forgot}>
                <div style={{ width: '70%' }}>
                    <Link to="/forgot-password" className={classes.link}>
                        <Typography variant="subtitle2" style={{ color: 'black' }}>
                            ¿Olvidaste tu contraseña?
                        </Typography>
                    </Link>
                </div>
            </Grid>
            <Grid item xs={12} className={classes.item} style={{ paddingBottom: 30 }}>
                <GenericButton color="black" width="70%" text="Ingresar" toDo={validate} />
                {/* <Link to="/" className={classes.link}>
                    <Button variant="contained" color="primary" className={classes.primaryButton}>
                        ingresar
	                </Button>
                </Link> */}
            </Grid>
            <MaterialSnack open={openAlert} handleClose={handleAlert} message={message} />
        </Grid>
    );
}
