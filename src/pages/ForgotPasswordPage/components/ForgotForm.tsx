import { Grid, TextField, Typography } from '@material-ui/core';
import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import GenericButton from '../../../components/GenericButton/GenericButton';
import MaterialSnack from '../../../components/MaterialSnack/MaterialSnack';
const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        textField: {
            width: '70%'
        },
        item: {
            textAlign: 'center',
            padding: '15px 0px 0px 0px'
        },
        primaryButton: {
            marginTop: '1em',
            width: '70%'
        },
        companyName: {
            fontWeight: 'bold',
            padding: '15px 0px 0px 0px',
            textAlign: 'center'
        },
        spamText: {
            padding: '15px 0px 0px 0px',
            textAlign: 'center',
            maxWidth: '70%'
        },
        link: {
            color: 'black',
            /*  &:focus, &:hover, &:visited, &:link, &:active {
                 text-decoration: none; */
            '&:link': {
                textDecoration: 'none'
            },

        },
        forgot: {
            textAlign: 'end',
            display: 'flex',
            justifyContent: 'center',
            padding: '15px 0px 0px 0px',
        },
    }),
);

export default function ForgotForm() {
    const classes = useStyles();
    const [email, setEmail] = React.useState("");
    const [openAlert, setOpenAlert] = React.useState(false);
    const [message, setMessage] = React.useState("");
    const handleAlert = () => {
        setOpenAlert(!openAlert);
    };


    const validate = () => {
        const emailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        if (emailRegex.test(email)) {
            //alert('correo valido');
            setMessage("Correo Valido");
            handleAlert();
        }
        else {
            setMessage("Correo electrónico incorrecto");
            handleAlert();
        }

    }

    
    return (
        <Grid container direction="row"
            justify="center"
            alignItems="center" >
            <Grid item xs={12} className={classes.companyName}>
                <Typography variant="h3" display="inline" style={{ fontWeight: 'bold' }}>
                    ¿Olvidaste tu contraseña?
                </Typography>
            </Grid>
            <Grid item xs={12} className={classes.spamText}>
                <Typography variant="subtitle1" display="inline" >
                    Ingresa tu correo electrónico y te enviaremos un link para recuperar tu contraseña.
                </Typography>
            </Grid>
            <Grid item xs={12} className={classes.item}>
                <TextField
                    onChange={(e) => { setEmail(e.target.value) }}
                    id="outlined-basic"
                    className={classes.textField}
                    label="Correo"
                    margin="normal"
                    variant="outlined"
                />
            </Grid>
            <Grid item xs={12} className={classes.item} style={{ paddingBottom: 30 }}>
                <GenericButton color="black" width="70%" text="Recuperar contraseña" toDo={validate} />
            </Grid>
            <MaterialSnack open={openAlert} handleClose={handleAlert} message={message} />
        </Grid>
    );
}