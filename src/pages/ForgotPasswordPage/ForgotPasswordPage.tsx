import { Grid, Paper } from '@material-ui/core';
import React from 'react';
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles';
import ForgotForm from './components/ForgotForm';

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        paper: {
            width: '90%',
            border: '2px solid black',
            boxShadow: 'none',
            [theme.breakpoints.up('sm')]: {
                width: '60%',
            },
            [theme.breakpoints.up('md')]: {
                width: '40%',
            },
        }
    }),
);
export default function ForgotPasswordPage() {
    const classes = useStyles();
    return (
        <Grid container direction="row"
            justify="center"
            alignItems="center" style={{ paddingTop: 50 }}>
            <Paper className={classes.paper}>
                <ForgotForm />
            </Paper>
        </Grid>
    );
}