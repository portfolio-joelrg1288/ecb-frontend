import React from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import ForgotPasswordPage from '../pages/ForgotPasswordPage/ForgotPasswordPage';
import LoginPage from '../pages/LoginPage/LoginPage';
import MainPage from '../pages/MainPage/MainPage';

export default function Routes() {
    return (
        <Router>
            <Switch>
                <Route exact path="/vehicles">
                    <MainPage />
                </Route>
                <Route exact path="/" >
                    <LoginPage />
                </Route>
                <Route exact path="/forgot-password" >
                    <ForgotPasswordPage />
                </Route>
            </Switch>
        </Router>
    );
}