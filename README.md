[![React](https://badgen.net/badge/React/v16.9.0/0e83cd?color=cyan)](https://flutter.dev/docs/development/tools/sdk/releases)

# ECB Frontend
ECB frontend for cars manage

## Info ##
This project uses:

* [React][1] as javascript library for web development

## Setup ##
In order to use this project, you need to install

* [nvm][2] to control node versions, last version

Fist, for setting up node js version and install dependencies:
```
nvm install
npm install
```

To start the application in development mode (load all services locally with hot-reload & REPL):
```
npm start
```

For creating a production build
```
npm run build
```

## Contribution guidelines ##

The rules to submit a contribution are:
* Write only on English
* Don't make push on master
* Do a rebase before merge request
* Request a review before merge
* Limit your text lines to 80 characters or fewer
* Add a break of line on any file
* Make atomic commits
* Follow the [git message][3] format using the regex:
```
((^[A-Z]{1})([a-z\ A-Z]+[a-z])(\n\n)((.)+([\n]{1,2})?)+)([\n\n]((Close:\ )|(See\ also:\ ) | (Resolves:\ ))\#[0-9]+)?
```

* Create a new branch before uploading any change, using the regex:
```
((feature)|((hot)?fix))\/([a-z]+(-?[a-z0-9]*)*)([a-z0-9])$
```

## TODO ##
- [ ] Test
- [ ] CI/CD

## Useful links
* [React Docs](https://es.reactjs.org/docs/getting-started.html)

[1]: https://es.reactjs.org
[2]: https://github.com/nvm-sh/nvm
[3]: https://thoughtbot.com/blog/better-commit-messages-with-a-gitmessage-template
